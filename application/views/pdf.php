<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
                
        foreach ($docs as $d){
        $mpdf = new \Mpdf\Mpdf(['format' => 'Legal', 'default_font_size' => 10,
	'default_font' => 'Arial']);
            // Ao invés de imprimir a view 'welcome_message' na tela, passa o código
            // HTML dela para a variável $html
            $nomeUsuario = $d->nomeA;
            $partes = explode(' ', $nomeUsuario);
            $ultimoNome = array_pop($partes). ", ";
            $resto = implode(" ", $partes);
            //echo $ultimoNome;
            //echo $resto;
            $nomeUsuario2 = $d->nomeA2;
            if ($nomeUsuario2){
                $partes2 = explode(' ', $nomeUsuario2);
                $ultimoNome2 = " I. ".array_pop($partes2). ", ";
                $resto2 = implode(" ", $partes2).".";
            }else{
                $ultimoNome2 = "";
                $resto2 = "";
            }
             //echo $ultimoNome2;
            //echo $resto2;
            $nomeUsuario3 = $d->nomeA3;
            if ($nomeUsuario3){
                $partes3 = explode(' ', $nomeUsuario3);
                $ultimoNome3 = " II. ".array_pop($partes3). ", ";
                $resto3 = implode(" ", $partes3).".";
            }else{
                $ultimoNome3 = "";
                $resto3 = "";
            }
            if ($nomeUsuario3){
                $ifrn = (" III. Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Norte. IV. Titulo. ");
            }elseif($nomeUsuario2){
                $ifrn = (" II. Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Norte. III. Titulo. ");
            }else{
                $ifrn = (" I. Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Norte. II. Titulo.");
            }
           
            $Coorientacao = $d->nomeC;
            if ($Coorientacao){
                $Coor = ($d->genCoor.": ");
            }else{
                $Coor = "";
            }
            $Subtitulo = $d->subtitulo;
            if ($Subtitulo){
                $sub = (": ".$Subtitulo);
            }else{
                $sub = "";
            }
            ////echo $ultimoNome3;
            //echo $resto3;
            
            $html = 
               
            "
                <style>
                    .tabela{
                        border: 1px solid black;
                        height:7.5cm;
                        width:12.5cm; 
                        margin:auto;
                        
                    }
                    .alinhar{
                    width:100%;
                    position: absolute;
                    top: 70%; /* posiciona na metade da tela */
                    margin-top: -25px; /* e retrocede metade da altura */
                    margin-left: -120px;
                    }
                    
                    .space{
                        text-indent:2em;
                    }
                    .space2{
                        text-align: right;
                        margin-right:10px;
                    }
                    .space3{
                        text-indent:0.3em;
                    }
                    .cutter{
                        padding-top:17px;
                        float:left;
                        width: 20px;
                    }
                    .lateral{
                        float:left; 
                    }
                    .span{margin-left:1cm;}
                    p{margin:1; padding-left:1cm;}
                    .semspa{padding-left:0cm;}
                    </style>
                    <div class='alinhar'>
                        <div class='tabela'><br><br>
                        <div class='cutter'>
                        <p class='semspa'>".$d->cutter."</p></div>
                        <div class='lateral'>
                        <p class='space3'>".$ultimoNome.$resto."</p></span>"
                        ."<p class='space'>".$d->titulo.$sub." / ".$d->nomeA."."." &ndash; "."$d->cidade, "."$d->ano"."</p>"
                        ."<p class='space'>".$d->pag." f. :".$d->ilustracao."."."</p><br>"
                        ."<p class='space'>".$d->tipo."(Técnico em ".$d->curso.")"." - "."Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Norte, "."$d->cidade, "."$d->ano".".</p>"
                        ."<p class='space'>".$d->genOrient.": ".$d->nomeO."</p>"
                        ."<p class='space'>".$Coor.$d->nomeC."</p><br>"
                        ."<p class='space'>".$d->assunto.$ultimoNome2.$resto2.$ultimoNome3.$resto3.$ifrn."</p>"
                        
                        ."<p class='space2'>"."CDU:".$d->CDU."</p></div>
                        </div><p align='center' style=' padding-left:0cm;'>Catalogação na Publicação elaborada pela Seção de Processamento Técnico<br> da
Biblioteca José de Arimatéia Pereira do IFRN.</p></div> ";
            // Define um Cabeçalho para o arquivo PDF
            $mpdf->SetHeader('');
            // Define um rodapé para o arquivo PDF, nesse caso inserindo o número da
            // página através da pseudo-variável PAGENO
            $mpdf->SetFooter('');
            // Insere o conteúdo da variável $html no arquivo PDF
            //$mpdf->writeHTML($html);
            // Cria uma nova página no arquivo
            $mpdf->AddPage();
            // Insere o conteúdo na nova página do arquivo PDF
            $mpdf->WriteHTML($html);
            // Gera o arquivo PDF
            $mpdf->Output();
        }
        ?>
    </body>
</html>
