<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function index()
	{
		$this->load->model('Form_model');
                $id = $this->uri->segment(3);
                $dados['posts'] = $this->Form_model->recuperar();
		$this->load->view('nav_2');
                $this->load->view('lista_fichas', $dados);
                $this->load->view('rodape');
                
	}
	public function pesquisa(){
		$this->load->model('Form_model');
		$filtro = $_GET['filtro'];
		if($filtro == "todos"){
           redirect("admin");
        }
		$dados['posts']=$this->Form_model->pesquisa($filtro);
		$dados['posts2']=$this->Form_model->recuperar();
		
		$this->load->view('nav_2');
		$this->load->view('result-fichas', $dados);
		$this->load->view('rodape');
	}
	public function pesquisa2(){
		$this->load->model('Form_model');
		$dados['posts']=$this->Form_model->buscar();
		
		$this->load->view('nav_2');
		$this->load->view('result-fichas', $dados);
		$this->load->view('rodape');
	}
	
}
