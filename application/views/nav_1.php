<!DOCTYPE html>
<html lang="pt">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Catalogue</title>
    
    <link rel="icon" type="image/png" href="<?=base_url()?>/assets/login/imagens/icons/logo.png"/>
    <!-- Bootstrap core CSS -->
    <link href="<?= base_url()?>/assets/front_inicial/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?= base_url()?>/assets/front_inicial/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    
    <!-- Plugin CSS -->
    <link href="<?= base_url()?>/assets/front_inicial/vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="<?= base_url()?>/assets/front_inicial/css/freelancer.css" rel="stylesheet">

    
        
  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="<?= base_url()?>aluno">Catalogue</a>
        
        <div class=" navbar-collaps" id="navbarResponsive" style="flex-grow:1;">
             <ul class="navbar-nav ml-auto" style="float:right;  margin-right:10px;">
            <li class="dropdown nav-item mx-0 mx-lg-1 " >
                <a class="nav-link py-3 px-0 px-lg-3 dropdown-toggle" id="usuario-nome_usual" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                
              </a>
                    <ul class="dropdown-menu" style='background-color:white; position:absolute; border: 1px solid #ccc; width:250px;'>
                        <li>
                            <div class="navbar-login">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <p class="text-center">
                                             <img id="foto" src="">
                                        </p>
                                    </div>
                                    <div class="col-lg-8" >
                                        <p class="text-left"id="usuario-tipo_vinculo"><strong></strong></p>
                                        <p class="text-left small" id="usuario-mat"></p>
                                        
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="navbar-login navbar-login-session">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>
                                            <a id="botao-sair" class="btn btn-danger btn-block">Sair</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
            
            <input type="hidden" value="<?=base_url();?>" name="base_url" id="base_url">
          <!--<ul class="navbar-nav ml-auto">
            <li class="dropdown nav-item mx-0 mx-lg-1 " >
                <a class="nav-link py-3 px-0 px-lg-3 dropdown-toggle" id="usuario-nome_usual" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                
              </a>

              <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" id="botao-sair" style="cursor: pointer;">Sair</a>
                <a class="dropdown-item" href="<?= base_url()?>formulario/login">Aluno</a>
              </div>
            </li>
          </ul>-->
        </div>
      </div>
    </nav>
    <script src="<?=base_url()?>/assets/front_inicial/vendor/jquery/jquery.min.js"></script>
    <script src="<?=base_url()?>/assets/front_inicial/vedor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?=base_url()?>/assets/js/funcionalidades.js"></script>
      <script src="<?= base_url()?>/assets/front_inicial/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url()?>/assets/front_inicial/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?= base_url()?>/assets/front_inicial/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?= base_url()?>/assets/front_inicial/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="<?= base_url()?>/assets/front_inicial/js/jqBootstrapValidation.js"></script>
    <script src="<?= base_url()?>/assets/front_inicial/js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?= base_url()?>/assets/front_inicial/js/freelancer.min.js"></script>
