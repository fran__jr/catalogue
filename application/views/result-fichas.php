<html>
<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>/assets/css/style.css">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Mali" rel="stylesheet">    
</head>
<style>
@media (min-width: 992px) {
.pesquisa {
  float: right;
}
}
</style>

<body>
<?php $qtdfichas=0;?>
<?php $qtdfichas2=0;?>


  <div class="container" >
     <form style="display: inline-block;" action="<?=base_url();?>admin/pesquisa" method="get">
            <label>Filtrar: </label>
            <button class="btn" type="submit" name="filtro" value ="hoje">Hoje</button>
            <button class="btn" type="submit" name="filtro" value ="semana">Esta Semana</button>
            <button class="btn" type="submit" name="filtro" value ="mes">Este mês</button>
            <button class="btn" type="submit" name="filtro" value ="ano">Este ano</button>
            <button class="btn" type="submit" name="filtro" value ="todos">Todos</button>
          </form> 
          <form style="display: inline-block; " class="pesquisa" action="<?=base_url();?>admin/pesquisa2" method="post">
            <input style=" margin-right: 10px;" class="form-control col-md-7" type="text" name="filtro_nome">
            <button class="btn" type="submit">Pesquisar</button> 
          </form> 

    <div class="row">
      <div class='col-md-6'>
        <div class="row">
         <h2 class="col-md-12 titulo">Registros Solicitados </h2>
       </div>
       <?php foreach ($posts as $p){?>
        <?php 
        $nomeA2 = $p->nomeA2;
        if ($nomeA2){
          $nome2 = ($nomeA2."; ");
        }else{
         $nome2 = ("");
       }
       $nomeA3 = $p->nomeA3;
       if ($nomeA3){
        $nome3 = ($nomeA3."; ");
      }else{
       $nome3 = ("");
     }?>

     <?php if($p->atualizacao==0 & $p->negacao==0){   
      $qtdfichas+=1;?>
       <div class=" panel panel-second">
        <div class="panel-body">
          <div class="row"><div class="col-md-8"><b>Título: </b><?php echo $p->titulo ;?></div>
        </div><hr>
        <div class="row">
          <div class="col-md-9"><b>Autores: </b><?php echo $p->nomeA."; ".$nome2.$nome3 ;?></div>
        </div><hr>

        <div class="row">
          <div class="col-md-8"><b>Tipo: </b><?php echo $p->tipo ;?></div>
        </div>
      </div>
           <button class="btn btn-primary btn-block" style="background-color: #ffa44a !important;border-color: #ffa44a !important;" onclick="location.href='<?=base_url()?>formulario/form_a/<?=$p->id?>'" >Acessar Ficha</button>
    </div>
    <input type="hidden" value="<?=base_url();?>" name="base_url" id="base_url">
  <?php }?><?php }?> 
  <?php if ($qtdfichas==0){?>
    <div class="alert alert-warning">
          <h4>Você não possui fichas catalográficas solicitadas</h4>
        </div>
  <?php }?>


</div>
<div class='col-md-6'>
  <div class='row'>
    <h2 class="col-md-12 titulo" style="color:#003333">Aprovados </h2>
  </div>
  <?php foreach ($posts as $p){?>
   <?php 
        $nomeA2 = $p->nomeA2;
        if ($nomeA2){
          $nome2 = ($nomeA2."; ");
        }else{
         $nome2 = ("");
       }
       $nomeA3 = $p->nomeA3;
       if ($nomeA3){
        $nome3 = ($nomeA3."; ");
      }else{
       $nome3 = ("");
     }?>

    <?php if($p->atualizacao==1){ $qtdfichas2+=1;?>

     <div class="panel panel-primary ">
      <div class="panel-body ">
        <div class="row"><div class="col-md-8"><b>Título: </b><?php echo $p->titulo ;?></div>
      </div><hr>
      <div class="row">
        <div class="col-md-9"><b>Autores: </b><?php echo $p->nomeA."; ".$nome2.$nome3 ;?></div>
      </div><hr>

      <div class="row">
        <div class="col-md-8"><b>Tipo: </b><?php echo $p->tipo ;?></div>
      </div>
    </div>
    <button class="btn btn-primary btn-block" onclick="location.href='<?=base_url()?>formulario/form_a/<?=$p->id?>'" >Acessar Ficha</button>
  </div>
  <input type="hidden" value="<?=base_url();?>" name="base_url" id="base_url">
<?php }?><?php }?>
<?php if ($qtdfichas2==0){?>
    <div class="alert alert-warning">
          <h4>Você não possui fichas catalográficas aprovadas</h4>
        </div>
  <?php }?>
</div>
<script src="<?=base_url()?>/assets/front_inicial/vendor/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>/assets/js/funcionalidades_admin.js"></script>
</div>
</div>

</body>
</html>