<!DOCTYPE html>
<html>
<head>
	<title>Preencher formulário</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>/assets/css/style.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <form method="post" enctype="multipart/form-data" action="<?=base_url();?>formulario/atualizar">
	<div class="container">
            <?php foreach ($posts as $p){?>
		<h2 class="titulo">Ficha Registrada</h2>
			<div class="panel panel-default ">
				<div class="form-group panel-body">
					<h4 class="bold text-left">Informações do autor(es) e orientador</h4>
                                        <input type="hidden" name="id" value="<?= $p->id; ?>"/>
					<div class="form-group row">
						
						<div class="col-md-6">
							<label>Nome Autor</label>
				      		<input name="nome_autor" placeholder="Nome completo do autor 1" type="text" value="<?php echo $p->nomeA; ?>" class="form-control">
				  		</div>
				  		<div class="col-md-4">
				  			<label>Nome Orientador(a)</label>
				      		<input name="nome_orientador" placeholder="Nome completo do orientador" type="text" value="<?php echo $p->nomeO; ?>" class="form-control">
				  		</div>
				  		<label class="radio-inline"  style = "padding-top:30px;">
						    <input type="radio" required name="genOrient" value="Orientadora" <?php if($p->genOrient == "Orientadora") echo "checked"?>> Feminino
						</label>
						<label class="radio-inline" style = "padding-top:30px;">
						    <input type="radio" required name="genOrient" value="Orientador" <?php if($p->genOrient == "Orientador") echo "checked"?>> Masculino
						</label>
				    </div>
				    <div class="form-group row">
						
						<div class="col-md-6">
							<label>Nome autor 2</label>
				      		<input name="nome_autor2" placeholder="Não possui autor 2" type="text" value="<?php echo $p->nomeA2; ?>" class="form-control">
				  		</div>
				  		<div class="col-md-4">
				  			<label>Nome Cooorientador(a)</label>
				      		<input name="nome_coorientador" placeholder="Nome completo do coorientador" type="text" value="<?php echo $p->nomeC; ?>" class="form-control">
				  		</div>
				  		<label class="radio-inline"  style = "padding-top:30px;">
						    <input type="radio" name="genCoor" value="Coorientadora" <?php if($p->genCoor == "Coorientadora") echo "checked"?>> Feminino
						</label>
						<label class="radio-inline"  style = "padding-top:30px;">
						    <input type="radio" name="genCoor" value="Coorientador" <?php if($p->genCoor == "Coorientador") echo "checked"?>> Masculino
						</label>
				    </div>
				    <div class="form-group row">
						
						<div class="col-md-6">
							<label>Nome autor 3</label>
				      		<input name="nome_autor3" placeholder="Não possui autor 3" type="text" value="<?php echo $p->nomeA3; ?>" class="form-control">
				  		</div>
				  		<div class="col-md-3">
				  			<label>Curso</label>
				      		<select class="form-control" required id="sel1" name="curso">
				      			<option value="" disabled selected>Curso</option>
				      			<option value="Comércio" <?php if($p->curso == "Comércio")echo "selected" ?>>Comércio</option>
						        <option value="Eletrônica" <?php if($p->curso == "Eletrônica")echo "selected" ?>>Eletrônica</option>
						        <option value="Informática para Internet" <?php if($p->curso == "Informática para Internet")echo "selected" ?>>Informática para Internet</option>
						    </select>
				  		</div>
				  		<div class="col-md-3">
				  			<label>Cidade</label>
				      		<input name="cidade" placeholder="Cidade" type="text" value="<?php echo $p->cidade; ?>" class="form-control">
				  		</div>
					</div>
				</div>
			</div>
			<!--continuar-->
			<div class="panel panel-default ">
				<div class="form-group panel-body">
					<h4 class="bold text-left">Informações sobre o projeto</h4>
					<div class="form-group row">
						
						<div class="col-md-5">
							<label>Titulo</label>
				      		<input name="titulo" placeholder="Título do trabalho" type="text" value="<?php echo $p->titulo; ?>" class="form-control">
				  		</div>
				  		<div class="col-md-5">
				  			<label>Subtítulo</label>
				      		<input name="subtitulo" placeholder="Subtítulo do trabalho" type="text" value="<?php echo $p->subtitulo; ?>" class="form-control">
				  		</div>
				  		<div class="col-md-2">
				  			<label>Ano</label>
				      		<input name="ano" placeholder="Ano" min="0" type="number" value="<?php echo $p->ano; ?>" class="form-control">
				  		</div>
				    </div>
				    <div class="form-group row">
						<div class="col-md-3">
							<label>Qtd de páginas</label>
				      		<input name="pag" placeholder="Qtd de páginas" min="0" type="number" value="<?php echo $p->pag; ?>" class="form-control">
				  		</div>
				  		<div class="col-md-5">
				  			<label>Tipo</label>
				      		<select class="form-control" id="sel1" name="tipo">
				      			<option value="">Tipo</option>
						        <option value="Trabalho de Conclusão de Curso" <?php if($p->tipo == "Trabalho de Conclusão de Curso")echo "selected" ?>>TCC</option>
						        <option value="Dissertação" <?php if($p->tipo == "Dissertação")echo "selected" ?>> Dissertação</option>
						    </select>
				  		</div>
				  		<div class="col-md-4">
				  			<label>Ilustrado? </label><br>
				      		<label class="radio-inline">
						      <input type="radio"  name="ilustracao" value="il" <?php if($p->ilustracao == "il")echo "checked=\"checked\"" ?>>Sim
						    </label>
						    <label class="radio-inline">
						      <input type="radio"  name="ilustracao" value=" "<?php if($p->ilustracao == " ")echo "checked=\"checked\"" ?>>Não
						    </label>
				  		</div>
					</div>
					<div class="form-group row">
						<div class="col-md-12">
							<label>Assunto</label>
  							<textarea class="form-control" rows="5" id="comment" name="assunto" placeholder="Assunto (min. 1 e máx. 5)"><?php echo $p->assunto; ?></textarea>
				  		</div>
				    </div>
				</div>
			</div>
                        <div class="panel panel-default ">
                            <div class="form-group panel-body">
                                <div class="form-group row">
                                    <div class="col-md-5">
                                         <input type="text" id="CDU" name="CDU" class="form-control" value="<?php if ($p->CDU) echo $p->CDU?>" placeholder="CDU" required >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-5">
                                        <input type="text" id="cutter" name="cutter" class="form-control" value="<?php if ($p->cutter) echo $p->cutter?>" placeholder="Cutter" required >
                                    </div>
                                </div>
                                </div>
                            </div>    
			<div class="panel panel-default">
	            <div class="form-group panel-body">
	            	<h4 class="bold text-left">Arquivo Anexado</h4>
	                <?php foreach ($posts as $p){?>
                            <u><a href=<?php echo base_url("assets/uploads/". $p->arquivo)?> target="_blank"><?php echo $p->titulo.".pdf"?></a></u>
                                <?php }?>
	            </div>				
			</div>
                        <div id="divTeste"></div>
                        <?php if(($p->atualizacao)==0){?>
							<button type="submit" class="btn btn-md bt-form" name="enviar" style="float:right;"value="enviar">APROVAR</button>
							<button type="button" id="negar" name="enviar"onclick="caixadeTexto()" style="float:right; margin-right:20px;" class="btn btn-md bt-form3" >NEGAR</button>
                        	<?php }else{ ?>
                        		<a class="btn btn-primary btn-block" style="text-decoration:none;"href="<?=base_url()?>aluno/pdf/<?=$p->id?>" target="_blank">Gerar Ficha</a>
                        		
                        	<?php }?>
        <?php } ?>
                        <input type="hidden" value="<?=base_url();?>" name="base_url" id="base_url">
    </div>
    </form>
    <script>
        function caixadeTexto(){
           var negar = document.getElementById('negar');
           var req1 = document.getElementById('CDU');
           var req2 = document.getElementById('cutter');
           var motivo = document.getElementById('motivo');
           req1.removeAttribute('required');
           req2.removeAttribute('required');
            document.getElementById('divTeste').innerHTML = '<div class="panel panel-default ">'+
                            '<div class="form-group panel-body">'+
                            '<h4 class="bold text-left">Motivo da negação</h4>'+
                                '<div class="form-group row">'+
                                   '<div class="col-md-12">'+
  							'<textarea class="form-control" rows="5" id="motivo" name="motivo" placeholder="Motivo" required></textarea>'+
				  		'</div>'+
                                '</div>'+
                                '</div>'+
                        '</div>';
           negar.setAttribute("onclick", "setNegarToSubmit()");
        }
        function setNegarToSubmit() {
           var motivo = document.getElementById('motivo');
           var negar = document.getElementById('negar');
           negar.type = "submit";
        }
    </script>
</body>
</html>