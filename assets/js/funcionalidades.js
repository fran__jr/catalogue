$(document).ready(function(){
    var base_url = $("#base_url").val();
	$.ajax({
			headers: { 
				"Authorization" : "JWT "+sessionStorage.getItem("token")
			},
			url: "https://suap.ifrn.edu.br/api/v2/minhas-informacoes/meus-dados/",
			contentType: 'application/json',
			dataType: 'json',
			type: 'GET',
			success: function (data) {
				$("#usuario-nome_usual").html(data.nome_usual);
				$("#usuario-tipo_vinculo").html(data.tipo_vinculo);
				$("#usuario-email").html(data.email);
				$("#usuario-mat").html(data.matricula);
				$("#foto").attr("src", "http://suap.ifrn.edu.br/"+data.url_foto_75x100);
				
			},
			error: function(data){
				alert("Impossível recuperar dados. Você deve fazer login!");
				window.location.href=base_url+"formulario";
			}
	});
		
		
});

$("#botao-sair").click(function(e){
    var base_url = $("#base_url").val();
	sessionStorage.removeItem("token");
	window.location.href= base_url+"formulario";
});

