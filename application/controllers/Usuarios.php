<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {
    
     public function salvarLogin()
	{
         $this->load->model('User_model');
         $vinculo = $this->uri->segment(3);
         $matricula = $this->uri->segment(4);
         $nome = $this->uri->segment(5);
         
         $u = $this->User_model->verificarMatEVin($matricula,$vinculo);
         if($u != NULL){
             $this->session->set_userdata("usuario", $u);
             $id_usuario = $this->session->userdata("usuario")->id;
             $dados['usuario'] = $this->User_model->recuperarUmUsuario($id_usuario);
             $vinculoUsuario = $dados['usuario']->vinculo;
             $matriculaUsuario = $dados['usuario']->mat;
             if(($matriculaUsuario == "20151041110293") or ($matriculaUsuario == "1730457")){
                 redirect("admin");           
             }elseif ($vinculoUsuario == "Aluno"){
                 redirect("aluno");
            }
             /*if($vinculoUsuario == "Aluno"){
                 redirect("aluno");           
             }elseif($vinculoUsuario == "Servidor"){
                 redirect("admin");
            }*/
         }else{
             $this->User_model-> nome = $nome;
             $this->User_model-> mat = $matricula;
             $this->User_model-> vinculo = $vinculo;
             $this->User_model-> salvarUsuario ();
             
             $u = $this->User_model->verificarMatEVin($matricula,$vinculo);
             $this->session->set_userdata("usuario", $u);
             $id_usuario = $this->session->userdata("usuario")->id;
             $dados['usuario'] = $this->User_model->recuperarUmUsuario($id_usuario);
             $vinculoUsuario = $dados['usuario']->vinculo;
             $matriculaUsuario = $dados['usuario']->mat;
             if(($matriculaUsuario == "20151041110293") or ($matriculaUsuario == "1730457")){
                 redirect("admin");           
             }elseif ($vinculoUsuario == "Aluno"){
                 redirect("aluno");
            }/*
             if($vinculoUsuario == "Aluno"){
                 redirect("aluno");           
             }elseif($vinculoUsuario == "Servidor"){
                 redirect("admin");
            }*/
            }
	}
    }

