<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aluno extends CI_Controller {

	public function index()
	{
		$this->load->view('nav_1');
                $this->load->model('Form_model');
                $id = $this->uri->segment(3);
                $dados['posts'] = $this->Form_model->recuperarFichaAluno();
                $this->load->view('lista_fichas_aluno', $dados);
                $this->load->view('rodape');

                
	}
    public function editar()
    {
        $this->load->model('Form_model');
        $id = $this->uri->segment(3);
        $dados['posts'] = $this->Form_model->recuperarUm($id);
        $this->load->view('nav_1');
        $this->load->view('form_aluno_editar', $dados);
        $this->load->view('rodape');
                
    }
         public function pdf()
	{
            $this->load->model("Form_model");
            $id = $this->uri->segment(3);
            $docs['docs']= $this->Form_model->recuperarUm($id);
            $this->load->view('pdf', $docs);
        }
}
