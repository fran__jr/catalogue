<!DOCTYPE html>
<html>
<head>
	<title>Preencher formulário</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>/assets/css/style.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
</head>

<body>
	<div class="container mudar">
		<h2 class="titulo">Registre sua ficha</h2>
	    <form method="post" enctype="multipart/form-data" action="<?=base_url();?>formulario/salvar">
			<div class="panel panel-default ">
				<div class="form-group panel-body">
					<h4 class="bold text-left">Informações do autor(es) e orientador</h4>
					<div class="form-group row">
						
						<div class="col-md-6">
				      		<input name="nome_autor" required placeholder="Nome completo do autor 1" type="text" class="form-control">
				  		</div>
				  		<div class="col-md-4">
				      		<input name="nome_orientador" required placeholder="Nome completo do orientador" type="text" class="form-control">
				  		</div>
				  		<label class="radio-inline">
						    <input type="radio" required name="genOrient" value="Orientadora"> Feminino
						</label>
						<label class="radio-inline">
						    <input type="radio" required name="genOrient" value="Orientador"> Masculino
						</label>
				    </div>
				    <div class="form-group row">
						
						<div class="col-md-6">
				      		<input name="nome_autor2" placeholder="Nome completo do autor 2 (opcional)" type="text" class="form-control">
				  		</div>
				  		<div class="col-md-4">
				      		<input name="nome_coorientador" placeholder="Nome completo do coorientador (opcional)" type="text" class="form-control">
				  		</div>
				  		<label class="radio-inline">
						    <input type="radio" name="genCoor" value="Coorientadora"> Feminino
						</label>
						<label class="radio-inline">
						    <input type="radio" name="genCoor" value="Coorientador"> Masculino
						</label>
				    </div>
				    <div class="form-group row">
						
						<div class="col-md-6">
				      		<input name="nome_autor3" placeholder="Nome completo do autor 3 (opcional)" type="text" class="form-control">
				  		</div>
				  		<div class="col-md-3">
				  			<select class="form-control" required id="sel1" name="curso">
				      			<option value="" disabled selected>Curso</option>
				      			<option value="Comércio">Comércio</option>
						        <option value="Eletrônica">Eletrônica</option>
						        <option value="Informática para Internet">Informática para Internet</option>
						    </select>
				  		</div>
				  		<div class="col-md-3">
				      		<input name="cidade" required placeholder="Cidade" type="text" class="form-control">
				  		</div>
					</div>
				</div>
			</div>
			<!--continuar-->
			<div class="panel panel-default ">
				<div class="form-group panel-body">
					<h4 class="bold text-left">Informações sobre o projeto</h4>
					<div class="form-group row">
						
						<div class="col-md-5">
				      		<input name="titulo" required placeholder="Título do trabalho" type="text" class="form-control">
				  		</div>
				  		<div class="col-md-5">
				      		<input name="subtitulo" placeholder="Subtítulo do trabalho (opcional)" type="text" class="form-control">
				  		</div>
				  		<div class="col-md-2">
				      		<input name="ano" required placeholder="Ano" type="number" min="0" class="form-control">
				  		</div>
				    </div>
				    <div class="form-group row">
						<div class="col-md-3">
				      		<input name="pag" required placeholder="Qtd de páginas" type="number" min="0" class="form-control">
				  		</div>
				  		<div class="col-md-5">
				      		<select class="form-control" required id="sel1" name="tipo">
				      			<option value="">Tipo</option>
						        <option value="Trabalho de Conclusão de Curso">TCC</option>
						        <option value="Dissertação"> Dissertação</option>
						    </select>
				  		</div>
				  		<div class="col-md-4">
				  			<label>Ilustrado? </label>
				      		<label class="radio-inline">
						      <input type="radio" required name="ilustracao" value="il">Sim
						    </label>
						    <label class="radio-inline">
						      <input type="radio"  name="ilustracao" required value=" ">Não
						    </label>
				  		</div>
					</div>
					<div class="form-group row">
						<div class="col-md-12">
  							<textarea class="form-control" required rows="5" id="comment" name="assunto" placeholder="Assunto (min. 1 e máx. 5)(Formato: 1.vdc. 2.vdc.)"></textarea>
				  		</div>
				    </div>
				</div>
			</div>
			<div class="panel panel-default">
                            <div class="form-group panel-body">
                                <h4 class="bold text-left">Anexar Arquivo PDF	</h4>
                                <div class="input-group input-file" name="Fichier1">
                                            <span class="input-group-btn">
                                            <button class="btn btn-default btn-choose" type="button">Procurar</button>
                                    </span>
                                    <input type="text" class="form-control" placeholder='Procurar arquivo...' />
                                    <span class="input-group-btn">
                                             <button class="btn btn-warning btn-reset" type="button">Resetar</button>
                                    </span>
                                </div>
                            </div>		
			</div>
			<button type="submit" class="btn btn-block btn-md bt-form">ENVIAR</button>
	    </form>
                <input type="hidden" value="<?=base_url();?>" name="base_url" id="base_url">
    </div>
    <script type="text/javascript">
        function bs_input_file() {
	$(".input-file").before(
		function() {
			if ( ! $(this).prev().hasClass('input-ghost') ) {
				var element = $("<input type='file' required name='arquivo' class='input-ghost' height:1'>");
				element.attr("name",$(this).attr("arquivo"));
				element.change(function(){
					element.next(element).find('input').val((element.val()).split('\\').pop());
				});
				$(this).find("button.btn-choose").click(function(){
					element.click();
				});
				$(this).find("button.btn-reset").click(function(){
					element.val(null);
					$(this).parents(".input-file").find('input').val('');
				});
				$(this).find('input').css("cursor","pointer");
				$(this).find('input').mousedown(function() {
					$(this).parents('.input-file').prev().click();
					return false;
				});
				return element;
			}
		}
	);
}
$(function() {
	bs_input_file();
});
    
    </script>
</body>
</html>
