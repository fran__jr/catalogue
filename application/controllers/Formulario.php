<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formulario extends CI_Controller {
	public function index()
	{
		$this->load->view('login');
        $this->load->view('rodape');
	}
        public function responder()
	{
		$this->load->view('nav_1');
                $this->load->view('form');
                $this->load->view('rodape');
	}
         public function form_a()
	{
                $this->load->model('Form_model');
                $id = $this->uri->segment(3);
                $dados['posts'] = $this->Form_model->recuperarUm($id);
		$this->load->view('nav_2');
                $this->load->view('form_adm', $dados);
                $this->load->view('rodape');
	}
          public function atualizar()
    {
             
                if (isset($_POST['motivo'])) {
                     $this->load->model('Form_model');
                    $this->Form_model->id= $_POST['id'];
                    $this->Form_model->titulo = $_POST['titulo'];
                    $this->Form_model->nomeA = $_POST['nome_autor'];
                    $this->Form_model->nomeA2 = $_POST['nome_autor2'];
                    $this->Form_model->nomeA3 = $_POST['nome_autor3'];
                    $this->Form_model->nomeO = $_POST['nome_orientador'];
                    $this->Form_model->nomeC = $_POST['nome_coorientador'];
                    $this->Form_model->cidade= $_POST['cidade'];
                    $this->Form_model->curso= $_POST['curso'];
                    $this->Form_model->ano = $_POST['ano'];
                    $this->Form_model->pag = $_POST['pag'];
                    $this->Form_model->assunto = $_POST['assunto'];
                    $this->Form_model->subtitulo = $_POST['subtitulo'];
                    $this->Form_model->ilustracao = $_POST['ilustracao'];
                    $this->Form_model->tipo = $_POST['tipo'];
                    $this->Form_model->cutter = $_POST['cutter'];
                    $this->Form_model->CDU = $_POST['CDU'];
                    $this->Form_model->motivo = $_POST['motivo'];
                    $this->Form_model->verificar= $_POST['enviar'];
                    $genOrient= $_POST['genOrient'];
                    $this->Form_model->genOrient = $genOrient;
                    if (isset($_POST['genCoor'])and ($_POST['nome_coorientador'])) {
                        $genCoor= $_POST['genCoor'];
                    }else{
                        $genCoor= "";
                    }
                    $this->Form_model->genCoor = $genCoor;
                    $this->Form_model->Update();
                    redirect('admin'); 
                }else{
                   
                    $this->load->model('Form_model');
                    $this->Form_model->id= $_POST['id'];
                    $this->Form_model->titulo = $_POST['titulo'];
                    $this->Form_model->nomeA = $_POST['nome_autor'];
                    $this->Form_model->nomeA2 = $_POST['nome_autor2'];
                    $this->Form_model->nomeA3 = $_POST['nome_autor3'];
                    $this->Form_model->nomeO = $_POST['nome_orientador'];
                    $this->Form_model->nomeC = $_POST['nome_coorientador'];
                    $this->Form_model->cidade= $_POST['cidade'];
                    $this->Form_model->curso= $_POST['curso'];
                    $this->Form_model->ano = $_POST['ano'];
                    $this->Form_model->pag = $_POST['pag'];
                    $this->Form_model->assunto = $_POST['assunto'];
                    $this->Form_model->subtitulo = $_POST['subtitulo'];
                    $this->Form_model->ilustracao = $_POST['ilustracao'];
                    $this->Form_model->tipo = $_POST['tipo'];
                    $this->Form_model->cutter = $_POST['cutter'];
                    $this->Form_model->CDU = $_POST['CDU'];
                    $this->Form_model->verificar= $_POST['enviar'];
                    $genOrient= $_POST['genOrient'];
                    $this->Form_model->genOrient = $genOrient;
                    if (isset($_POST['genCoor']) and ($_POST['nome_coorientador'])) {
                        $genCoor= $_POST['genCoor'];
                    }else{
                        $genCoor= "";
                    }
                    $this->Form_model->genCoor = $genCoor;
                    $this->Form_model->Update();
                    redirect('admin'); 
                }
    }
          public function atualizarAluno()
    {
                $this->load->model('Form_model');
                $this->Form_model->id= $_POST['id'];
                $this->Form_model->titulo = $_POST['titulo'];
                $this->Form_model->nomeA = $_POST['nome_autor'];
                $this->Form_model->nomeA2 = $_POST['nome_autor2'];
                $this->Form_model->nomeA3 = $_POST['nome_autor3'];
                $this->Form_model->nomeO = $_POST['nome_orientador'];
                $this->Form_model->nomeC = $_POST['nome_coorientador'];
                $this->Form_model->cidade= $_POST['cidade'];
                $this->Form_model->curso= $_POST['curso'];
                $this->Form_model->ano = $_POST['ano'];
                $this->Form_model->pag = $_POST['pag'];
                $this->Form_model->assunto = $_POST['assunto'];
                $this->Form_model->subtitulo = $_POST['subtitulo'];
                $this->Form_model->ilustracao = $_POST['ilustracao'];
                $this->Form_model->tipo = $_POST['tipo'];
                $genOrient= $_POST['genOrient'];
                $this->Form_model->genOrient = $genOrient;
                if (isset($_POST['genCoor'])and ($_POST['nome_coorientador'])) {
                    $genCoor= $_POST['genCoor'];
                }else{
                    $genCoor= "";
                }
                $this->Form_model->genCoor = $genCoor;
                $this->Form_model->UpdateAluno();
                redirect('aluno'); 
    }
     
        public function fichas()
	{
                
                
	}
        public function salvar()
	{
                
		$this->load->model('Form_model');
                $this->Form_model->usuario_id = $this->session->userdata("usuario")->id;
                $nomeA= $_POST['nome_autor'];
                $this->Form_model->nomeA = $nomeA;
                $nomeA2= $_POST['nome_autor2'];
                $this->Form_model->nomeA2 = $nomeA2;
                $nomeA3= $_POST['nome_autor3'];
                $this->Form_model->nomeA3 = $nomeA3;
                $nomeO= $_POST['nome_orientador'];
                $this->Form_model->nomeO = $nomeO;
                $nomeC= $_POST['nome_coorientador'];
                $this->Form_model->nomeC = $nomeC;
                $cidade= $_POST['cidade'];
                $this->Form_model->cidade= $cidade;
                $curso= $_POST['curso'];
                $this->Form_model->curso= $curso;
                $ano= $_POST['ano'];
                $this->Form_model->ano = $ano;
                $pag= $_POST['pag'];
                $this->Form_model->pag = $pag;
                $assunto= $_POST['assunto'];
                $this->Form_model->assunto = $assunto;
                $titulo= $_POST['titulo'];
                $this->Form_model->titulo = $titulo;
                $subtitulo= $_POST['subtitulo'];
                $this->Form_model->subtitulo = $subtitulo;
                $ilustracao= $_POST['ilustracao'];
                $this->Form_model->ilustracao = $ilustracao;
                $tipo= $_POST['tipo'];
                $this->Form_model->tipo = $tipo;
                $genOrient= $_POST['genOrient'];
                $this->Form_model->genOrient = $genOrient;
                if (isset($_POST['genCoor'])and ($_POST['nome_coorientador'])) {
                    $genCoor= $_POST['genCoor'];
                }else{
                    $genCoor= "";
                }
                $this->Form_model->genCoor = $genCoor;
                
                $nome          = $titulo;
                $arquivo    = $_FILES['arquivo'];
                $configuracao = array(
                'upload_path'   => './assets/uploads/',
                'allowed_types' => 'pdf',
                'file_name'     => $nome,
                'max_size'      => '0'
            );      
            $this->load->library('upload');
            $this->upload->initialize($configuracao);
            if ($this->upload->do_upload('arquivo'))
            {
                $info_arquivo = $this->upload->data();
                $arquivo= $info_arquivo['file_name'];
                $this->load->model("Form_model");
                $this->Form_model->titulo = $nome;
                $this->Form_model->arquivo = $arquivo;
                $this->Form_model->inserir();
                redirect('aluno');
            }
            else
            {
            echo $this->upload->display_errors();
            }
                
	}
}
