<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Catalogue</title>

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url()?>/assets/front_inicial/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?= base_url()?>/assets/front_inicial/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    
    <!-- Plugin CSS -->
    <link href="<?= base_url()?>/assets/front_inicial/vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">

    <link href="<?= base_url()?>/assets/fonts/glacial-indifference/GlacialIndifference-Regular.otf" rel="stylesheet" type="text/css">
    <link href="<?= base_url()?>/assets/fonts/glacial-indifference/GlacialIndifference-Bold.otf" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="<?= base_url()?>/assets/front_inicial/css/freelancer.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Catalogue</a>
        <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#portfolio">Equipe</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#about">Sobre</a>
            </li>
        
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#contact">Contato</a>
            </li>
            <li class="dropdown nav-item mx-0 mx-lg-1 ">
              <a class="nav-link py-3 px-0 px-lg-3" href="<?= base_url()?>formulario/login" role="button" aria-haspopup="true" aria-expanded="false">
                Login
              </a>

              
            </li>
          </ul>
        </div>
      </div>
    </nav>
