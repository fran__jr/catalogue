<?php

class User_model extends CI_Model{
    public $nome;
    public $mat;
    public $vinculo;
    public $id;
    
    public function __construct() {
        parent::__construct();
    }
    public function salvarUsuario(){
            $dados = array(
				"nome"=>$this->nome,
				"vinculo"=>$this->vinculo,
				"mat"=>$this->mat
				
				);
			return $this->db->insert('catalogue_usuario', $dados);
    	}

    public function verificarMatEVin($matricula,$vinculo){
	        $this->db->where("mat", $matricula);
	        $this->db->where("vinculo", $vinculo);
	        $r = $this->db->get("catalogue_usuario"); 
	        return $r->row();
    }
    public function recuperarUmUsuario($id){
            $this->db->where('id', $id); 
	        $query = $this->db->get('catalogue_usuario');
	        return $query->row();
    	}

}
