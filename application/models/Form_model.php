<?php

class Form_model extends CI_Model{
    public $id;
    public $nomeA;
    public $nomeA2;
    public $nomeA3;
    public $nomeC;
    public $nomeO;
    public $cidade;
    public $curso;
    public $titulo;
    public $subtitulo;
    public $pag;
    public $ano;
    public $assunto;
    public $datahora;
    public $ilustracao;
    public $tipo;
    public $arquivo;
    public $cutter;
    public $CDU;
    public $usuario_id;
    public $verificar;
    public $motivo;
    public $genCoor;
    public $genOrient;
    
 
    public function __construct() {
        parent::__construct();
    }
    public function inserir() {
        $dados = array("nomeA" => $this->nomeA, "nomeA2" => $this->nomeA2,"nomeA3" => $this->nomeA3, "nomeC" => $this->nomeC, "nomeO" => $this->nomeO,
            "cidade" => $this->cidade, "curso" => $this->curso, "titulo" => $this->titulo, "subtitulo" => $this->subtitulo, "ano" => $this->ano, "pag" => $this->pag,
            "assunto" => $this->assunto, "ilustracao" => $this->ilustracao, "tipo" => $this->tipo, "arquivo" =>$this-> arquivo, "usuario_id" => $this->usuario_id, "genCoor" => $this->genCoor, "genOrient" => $this->genOrient);
        $this->db->insert('catalogue_post_form', $dados);
        return $this->db->insert_id();
    }
    public function recuperar() {
        $this->db->order_by("datahora", "desc");
        $query = $this->db->get('catalogue_post_form');
        return $query->result();
    }
    public function recuperarFichaAluno() {
        $this->db->where('usuario_id', $this->session->userdata('usuario')->id);
        $query = $this->db->get('catalogue_post_form');
        return $query->result();
    }
    public function recuperarUm($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('catalogue_post_form');
        return $query->result();
    }
    public function update() {
        $verificar=$this->verificar;
        if($verificar=="enviar"){
            $atualizar="atualizacao";
        }else{
            $atualizar="negacao";
            $motivo = "motivo";
        }
        $dados = array("nomeA" => $this->nomeA, "nomeA2" => $this->nomeA2,"nomeA3" => $this->nomeA3, "nomeC" => $this->nomeC, "nomeO" => $this->nomeO,
            "cidade" => $this->cidade, "curso" => $this->curso, "titulo" => $this->titulo, "subtitulo" => $this->subtitulo, "ano" => $this->ano, "pag" => $this->pag,
            "assunto" => $this->assunto, "ilustracao" => $this->ilustracao, "tipo" => $this->tipo, "cutter" => $this->cutter, "CDU" => $this->CDU,  $atualizar => 1, "motivo"=>$this->motivo, "genCoor" => $this->genCoor, "genOrient" => $this->genOrient, "datahora"=> $this->datahora);
        $this->db->set($dados);
        $this->db->where('id', $this->id);
        $this->db->update('catalogue_post_form', $dados);
    }
    public function updateAluno() {
        $dados = array("nomeA" => $this->nomeA, "nomeA2" => $this->nomeA2,"nomeA3" => $this->nomeA3, "nomeC" => $this->nomeC, "nomeO" => $this->nomeO,
            "cidade" => $this->cidade, "curso" => $this->curso, "titulo" => $this->titulo, "subtitulo" => $this->subtitulo, "ano" => $this->ano, "pag" => $this->pag,
            "assunto" => $this->assunto, "ilustracao" => $this->ilustracao, "tipo" => $this->tipo, 'atualizacao' => 0, 'negacao' => 0,  "genCoor" => $this->genCoor, "genOrient" => $this->genOrient, "datahora"=> $this->datahora);
        $this->db->set($dados);
        $this->db->where('id', $this->id);
        $this->db->update('catalogue_post_form', $dados);
    }
    public function pesquisa($filtro) {
        //RECUPERA TUDO
        //"select date(datahora) from post_form"
        if ($filtro == "semana"){
            $query = $this->db->query ("select * from catalogue_post_form WHERE date(datahora) BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE() order by datahora desc");
        }elseif($filtro == "hoje"){
            $query = $this->db->query ("select * from catalogue_post_form WHERE DAY(date(datahora))= DAY(CURRENT_DATE()) AND MONTH(date(datahora))= MONTH(CURRENT_DATE()) AND YEAR(date(datahora))= YEAR(CURRENT_DATE())  order by datahora desc");
        }elseif($filtro == "mes"){
            $query = $this->db->query ("select * from catalogue_post_form WHERE MONTH(date(datahora))= MONTH(CURRENT_DATE()) AND YEAR(date(datahora))= YEAR(CURRENT_DATE())  order by datahora desc");
        }elseif($filtro == "ano"){
            $query=$this->db->query ("select * from catalogue_post_form WHERE YEAR(date(datahora))= YEAR(CURRENT_DATE())  order by datahora desc");
        }
        //POR SEMANA
        // $query = $this->db->query ("select datahora from post_form WHERE date(datahora) BETWEEN CURRENT_DATE()-7 AND CURRENT_DATE() ");
        //POR MES
       // $query = $this->db->query ("select datahora from post_form WHERE MONTH(date(datahora))= MONTH(CURRENT_DATE()) AND YEAR(date(datahora))= YEAR(CURRENT_DATE())");
        //POR ANO
        //$query = $this->db->query ("select datahora from post_form WHERE YEAR(date(datahora))= YEAR(CURRENT_DATE())");
        //POR DIA
        //$query = $this->db->query ("select datahora from post_form WHERE DAY(date(datahora))= DAY(CURRENT_DATE()) AND MONTH(date(datahora))= MONTH(CURRENT_DATE()) AND YEAR(date(datahora))= YEAR(CURRENT_DATE())");
        return $query->result();
    }
  public function buscar() {
    $termo=$this->input->post('filtro_nome');
   $query=$this->db->query ("select * from catalogue_post_form WHERE titulo LIKE '%$termo%' OR nomeA LIKE '%$termo%' OR nomeA2 LIKE '%$termo%' OR nomeA3 LIKE '%$termo%' OR nomeO LIKE '%$termo%'OR nomeC LIKE '%$termo%'");
    return $query->result();
    }
}
