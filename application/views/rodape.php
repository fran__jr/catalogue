<style>
  .footer-bottom {
    padding: 6px 0 6px;
    border-top: 1px solid #ccb;
    background: #ccb; 
    position: absolute;
    bottom: 0;
    width: 100%;
}
.social-link li {
    display: inline-block;
    margin: 0 5px;
}
@media (max-width: 766px) {
.copyright-text {
    text-align: center;
}
.social-link{
    width: 100%;
    text-align: center;
    padding-left: 0px;
    padding-top:9px;
}

</style>
<div class="footer-bottom">
<div class="container">
          <div class="row">
            <div class="col-sm-6 ">
              <div class="copyright-text">
               <img style="width:50px; margin-left:20px;"src="<?=base_url()?>/assets/img/fases-1.png">
               <a style="color: black; margin-bottom: 0;" href="http://fases.ifrn.edu.br/">fases.ifrn.edu.br</a>
              </div>
            </div> <!-- End Col -->
            <div class="col-sm-6">              
              <ul class="social-link pull-right">
                <li> <p style="color: black; margin-bottom: 0;">CopyRight © 2018 Catalogue</p></li> 
              </ul>             
            </div> <!-- End Col -->
          </div>
        </div>
</div>