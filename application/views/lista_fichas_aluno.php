<html>
<head>
    <meta charset="UTF-8">
    <title></title>
    <link href="<?=base_url()?>assets/css/bootstrap.min.css">
    
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>/assets/css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>
<body>

    <div class="container">
        <div class="row">
            <h2 class="col-md-10 titulo">Trabalhos Registrados</h2>
            <button onclick="location.href='<?=base_url()?>formulario/responder'"class="btn btn-outline btn-md bt-form2 col-md-2">Novo Registro</button>
            
            
        </div>
        <?php if (count($posts)==0){?>
        <div class="alert alert-warning">
          <h4>Você não possui fichas catalográficas solicitadas</h4>
        </div>
        <?php } else{?>
            
        <?php foreach ($posts as $p){?>
        <?php 
        $nomeA2 = $p->nomeA2;
        if ($nomeA2){
            $nome2 = ($nomeA2."; ");
        }else{
             $nome2 = ("");
        }
        $nomeA3 = $p->nomeA3;
        if ($nomeA3){
            $nome3 = ($nomeA3."; ");
        }else{
             $nome3 = ("");
        }?>
        <!-- <div class="panel panel-primary">
            <div class="panel-body">
                <div class="row"><div class="col-md-6"><b>TÃ­tulo: </b><?php echo $p->titulo ;?></div>
                </div><hr>
                <div class="row">
                    <div class="col-md-6"><b>Autores: </b><?php echo $p->nomeA."; ".$nome2.$nome3 ;?></div>
                </div><hr> -->
                
                
                    <?php if($p->atualizacao==1){?>
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="row"><div class="col-md-6"><b>Título: </b><?php echo $p->titulo ;?></div>
                            </div><hr>
                            <div class="row">
                                <div class="col-md-6"><b>Autores: </b><?php echo $p->nomeA."; ".$nome2.$nome3 ;?></div>
                            </div><hr>
                                <div class="row">
                                    <div class="col-md-6"><b>Status:</b><?php echo "<span style='color:#008B00'> Ficha Revisada" ?></div>
                                </div> 
                    <?php }?>
                    <?php if($p->atualizacao==0 & $p->negacao==1){?>                    
                    <div class="panel panel-third">
                        <div class="panel-body">
                            <div class="row"><div class="col-md-6"><b>Título: </b><?php echo $p->titulo ;?></div>
                        </div><hr>
                        <div class="row">
                            <div class="col-md-6"><b>Autores: </b><?php echo $p->nomeA."; ".$nome2.$nome3 ;?></div>
                        </div><hr>    
                        <div class="row">
                            <div class="col-md-6">
                                <b>Status:</b><?php echo "<span style='color:red'> Negado" ?>
                            </div> 
                        </div><hr>
                        <div class="row">
                            <div class="col-md-6"><b>Motivo:</b><?php echo $p->motivo ;?></div>
                        </div> 

                    <?php }?>
                    <?php if($p->atualizacao==0 & $p->negacao==0){?>                    
                    <div class="panel panel-second">
                        <div class="panel-body">
                            <div class="row"><div class="col-md-6"><b>Título: </b><?php echo $p->titulo ;?></div>
                        </div><hr>
                        <div class="row">
                            <div class="col-md-6"><b>Autores: </b><?php echo $p->nomeA."; ".$nome2.$nome3 ;?></div>
                        </div><hr>    
                        <div class="row">
                            <div class="col-md-6">
                                <b>Status:</b><?php echo "<span style='color:orange'> Aguardando avaliação" ?>
                            </div>
                        </div>

                    <?php }?>
               
                        
                        
                    
                
            </div>
            <?php if($p->atualizacao==1){?>
                <a class="btn btn-primary btn-block" style="text-decoration:none;"href="<?=base_url()?>aluno/pdf/<?=$p->id?>" target="_blank">Gerar Ficha</a>
            <?php }?>
            <?php if($p->atualizacao==0 & $p->negacao==1){?>
                <button class="btn btn-primary btn-block" style="background-color: #CD0000 !important;border-color: #CD0000 !important;" onclick="location.href='<?=base_url()?>aluno/editar/<?=$p->id?>'" >Acessar Ficha</button>
            <?php }?>
        </div>
        <input type="hidden" value="<?=base_url();?>" name="base_url" id="base_url">
        <?php }?>
        <script src="<?=base_url()?>/assets/front_inicial/vendor/jquery/jquery.min.js"></script>
         <?php }?>
</div>
</body>
</html>